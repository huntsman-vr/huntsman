# Huntsman VR #

Huntsman is a virtual reality game. You play as a primitive hunter in the prehistoric era. The goal of the game is to hunt animals using a spear and bring home food for the tribe. The spear can be used to throw and stab enemies. The player controls will be powered by the motion sensing capabilities of the HTC Vive. Naturally, the more food you bring home, the better. You will progress from hunting rabbits to hunting the mighty bear! Bringing back food for the tribe will be duly rewarded. An example would be to gain access to new more powerful weapons.

You hunt in an open-world hunting territory in which various wild animals are spawned. You need to look out for some animals because they can also hurt you. The hunter becomes the hunted if you are not careful. We want the animals to have situational awareness and become scared and run away if they see you, unless they believe they can defeat you. Staying hidden is important. This means the game will have some sort of stealth mechanic.

The environment should respond to to things like fire. The player can for instance scare away predators by using a torch. A campfire can be made to cook food. Keeping raw food around may attract unwanted attention of other predators.

There will also be survival aspects where animals seek food and water as part of their default behaviour, as they go about their day. The hunter must also take care of his survival needs, making sure to eat and drink enough.

### Getting started ###

To-do
* Unity installation

### Contribution guidelines ###

To-do

* Writing tests
* Code review
* Coding standards
* Other guidelines